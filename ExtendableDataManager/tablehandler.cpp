#include "tablehandler.h"

TableHandler::TableHandler(ReferenceInstancePtr<IDataBase> dataSource, Interface mainInterface)
{
	this->dataSource = dataSource;
	this->tableName = convertIterfaceToTableName(mainInterface);
	itemModel = nullptr;
	CreateTable();
}

TableHandler::~TableHandler()
{
	if(itemModel)
		delete itemModel;
}

bool TableHandler::HasRelation(Interface relationInterface)
{
	return relationTablesStructs.contains(relationInterface);
}

bool TableHandler::CreateTable()
{
	// Is name valid?
	if(tableName == nullptr || tableName == "")
	{
		qDebug() << "Tree name is empty!";
		return false;
	}

	coreTableStruct = {{"id", QVariant::Int}};

	if(IsTableExists(tableName))
	{
		// Is table has right structure.
		if(!IsTableHasRightStructure(tableName, coreTableStruct))
		{
			qDebug() << "Table" << tableName << "has wrong structure";
			return false;
		}
	}
	else
	{
		// Create table.
		QString queryStr = QString("CREATE TABLE IF NOT EXISTS %1 (%2)")
		        .arg(tableName)
		        .arg(GetHeaderString(coreTableStruct));
		QSqlQuery query = dataSource->ExecuteQuery(queryStr);
	}

	CombineWholeTableStruct();
	isCreated = true;
	return true;
}

bool TableHandler::SetRelation(Interface relationInterface, TableStructMap fields)
{
	// HACK: This kind of magic trick with "id" field made only for setting proper table header
	// because for setting relation I need "id" field in table but not for operating with data.
	fields.insert("id", QVariant::Int);
	QString databaseRelationName = QString("r_%1_%2")
	        .arg(tableName)
	        .arg(convertIterfaceToTableName(relationInterface));
	bool tableCreationNeeded = true;

	if(IsTableExists(databaseRelationName))
	{
		tableCreationNeeded = false;

		if(!IsTableHasRightStructure(databaseRelationName, fields))
		{
			tableCreationNeeded = true;
			DeleteRelation(relationInterface);
		}
	}

	if(tableCreationNeeded)
	{
		QString dataFields = GetHeaderString(fields, true);

		if(dataFields == "")
		{
			qDebug() << "Can't create relation!";
			return false;
		}

		QString queryStr = QString("CREATE TABLE IF NOT EXISTS %1 (%2)")
		        .arg(databaseRelationName)
		        .arg(dataFields);
		QSqlQuery queryResult = dataSource->ExecuteQuery(queryStr);
	}

	fields.remove("id");
	relationTablesStructs.insert(relationInterface, fields);

	if(itemModel)
		itemModel->AttachRelation(relationInterface, fields);

	return true;
}

bool TableHandler::DeleteRelation(Interface relationInterface)
{
	qDebug() << "DeleteRelation";
	QString queryStr = QString("DROP TABLE  r_%1_%2")
	        .arg(tableName)
	        .arg(convertIterfaceToTableName(relationInterface));
	QSqlQuery queryResult = dataSource->ExecuteQuery(queryStr);
	relationTablesStructs.remove(relationInterface);
	CombineWholeTableStruct();
	return true;
}

QString TableHandler::convertIterfaceToTableName(const Interface& interface)
{
	return QString(interface.iid()).replace('/', '_').replace('.', '_');
}

TableItem TableHandler::GetItem(int id)
{
	QString queryStr = QString("SELECT %1.id").arg(tableName);
	QStringList joinTables;
	QString tableRefPrefix = QString("r_%1_").arg(tableName);

	for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
	{
		queryStr.append(",");
		auto relationTableName = convertIterfaceToTableName(iter.key());
		queryStr.append( GetFieldsNames(tableRefPrefix+relationTableName, iter.value()) );
		joinTables.append(relationTableName);
	}

	queryStr.append( QString(" FROM %1 ").arg(tableName) );
	queryStr.append( QString(" ON %1.id = %2 ").arg(tableName).arg(id) );

	for(int i = 0; i < joinTables.count(); i++)
	{
		queryStr.append(QString(" LEFT JOIN r_%1_%2 ON %1.id = r_%1_%2.id")
		        .arg(tableName)
		        .arg(joinTables[i]));
	}

	QSqlQuery query = dataSource->ExecuteQuery(queryStr);
	TableItem buf;
	QString bufStr;
	int queryFieldNum;
	bufStr = "";
	queryFieldNum = 0;
	buf.id = query.value(queryFieldNum).toInt();
	++queryFieldNum;

	for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
	{
		qDebug() << "> " << iter.key();

		for(auto fieldIter = iter.value().begin(); fieldIter != iter.value().end(); ++fieldIter)
		{
			qDebug() << query.value(queryFieldNum);
			buf.dataChunks[iter.key()][fieldIter.key()] = query.value(queryFieldNum);
			++queryFieldNum;
		}
	}

	return buf;
}

QList<TableItem> TableHandler::GetData()
{
	QString queryStr = QString("SELECT %1.id").arg(tableName);
	QStringList joinTables;
	QString tableRefPrefix = QString("r_%1_").arg(tableName);

	for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
	{
		auto tableName = convertIterfaceToTableName(iter.key());
		queryStr.append(",");
		queryStr.append( GetFieldsNames(tableRefPrefix+tableName, iter.value()) );
		joinTables.append(tableName);
	}

	queryStr.append( QString(" FROM %1 ").arg(tableName) );

	for(int i = 0; i < joinTables.count(); i++)
	{
		queryStr.append(QString(" LEFT JOIN r_%1_%2 ON %1.id = r_%1_%2.id")
		        .arg(tableName)
		        .arg(joinTables[i]));
	}

	QSqlQuery query = dataSource->ExecuteQuery(queryStr);
	QList<TableItem> itemInfoList;
	TableItem buf;
	QString bufStr;
	int queryFieldNum;

	while (query.next())
	{
		bufStr = "";
		queryFieldNum = 0;
		buf.id = query.value(queryFieldNum).toInt();
		++queryFieldNum;

		for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
		{
			auto& dataCunk = buf.dataChunks[iter.key()];
			for(auto fieldIter = iter.value().begin(); fieldIter != iter.value().end(); ++fieldIter)
			{
				auto dbValue = query.value(queryFieldNum);
				switch(fieldIter.value().type())
				{
				case QVariant::DateTime:
					dbValue = QDateTime::fromSecsSinceEpoch(dbValue.toULongLong(), Qt::TimeSpec::UTC);
					break;
				case QVariant::Bool:
					dbValue = dbValue.toBool();
					break;
				}
				dataCunk[fieldIter.key()] = dbValue;
				++queryFieldNum;
			}
		}

		itemInfoList.append(buf);
		buf.dataChunks.clear();
	}

	return itemInfoList;
}

void TableHandler::InstallModel()
{
	if(!itemModel)
		itemModel = new ExtendableDataModel(this);

	auto relationStructIter = relationTablesStructs.begin();

	while(relationStructIter != relationTablesStructs.end())
	{
		itemModel->AttachRelation(relationStructIter.key(), relationStructIter.value());
		++relationStructIter;
	}

	itemModel->LoadData();
}

QPointer<IExtendableDataModel> TableHandler::GetModel()
{
	InstallModel();
	return itemModel;
}

int TableHandler::AddItem(TableItem& item)
{
	QString queryStr = QString("INSERT INTO %1 (id) VALUES (NULL)").arg(tableName);
	QSqlQuery query = dataSource->ExecuteQuery(queryStr);
	int lastId = query.lastInsertId().toInt();
	qDebug() << lastId;

	for(auto iter = item.dataChunks.begin(); iter != item.dataChunks.end(); ++iter)
	{
		QString valuesString = GetInsertValuesString(relationTablesStructs[iter.key()], lastId, iter.value());

		if(valuesString != "")
		{
			queryStr = "";
			queryStr.append(QString("INSERT INTO r_%1_%2 %3")
			        .arg(tableName)
			        .arg(convertIterfaceToTableName(iter.key()))
			        .arg(valuesString)
			);
			dataSource->ExecuteQuery(queryStr);
		}
		else
		{
			qDebug() << "Empty string!";
		}
	}

	return lastId;
}

bool TableHandler::UpdateItem(TableItem& item)
{
	QString queryStr;
	QStringList joinTables;

	for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
	{
		QString valuesString = GetUpdateValuesString(iter.value(), item.id);

		if(valuesString != "")
		{
			queryStr = "";
			queryStr.append(QString("UPDATE r_%1_%2 SET %3")
			        .arg(tableName)
			        .arg(convertIterfaceToTableName(iter.key()))
			        .arg(valuesString) );
			QList<QString> list = iter.value().keys();

			for(int i = 0; i < list.count(); ++i)
				list[i] = ":" + list[i].toUpper();

			QList<QVariant> values;
			for(auto& field : item.dataChunks[iter.key()].values())
			{
				switch(field.type())
				{
				case QVariant::DateTime:
					values.append(field.toDateTime().toSecsSinceEpoch());
					break;
				case QVariant::Bool:
					values.append(field.toBool());
					break;
				default:
					values.append(field);
				}
			}

			dataSource->ExecuteQuery(queryStr, &list, &values);
		}
		else
		{
			qDebug() << "Empty string!";
		}
	}

	return true;
}

bool TableHandler::DeleteItem(int id)
{
	QString queryStr = QString("delete from %1 where id=%2").arg(tableName).arg(id);
	qDebug() << "Delete Task" << queryStr;
	QSqlQuery query = dataSource->ExecuteQuery(queryStr);
	return true;
}

QMap<Interface, TableStructMap> TableHandler::GetHeader()
{
	return relationTablesStructs;
}

QString TableHandler::GetHeaderString(TableStructMap &tableStruct, bool createRelation)
{
	QString structStr = "";
	TableStructMap::Iterator i = tableStruct.begin();
	TableStructMap::Iterator lastElement = --tableStruct.end();

	while(i != tableStruct.end())
	{
		QString typeNameString = dataBaseTypesNames.contains(i.value().type()) ?
		        dataBaseTypesNames[i.value().type()] : QVariant::typeToName(i.value().type());
		structStr.append(QString("%1 %2").arg(i.key()).arg(typeNameString));

		if(i.key() == "id")
		{
			QString idAppendix = createRelation ?
			        QString(" REFERENCES %1(id) ON DELETE CASCADE").arg(tableName) :
			        " PRIMARY KEY AUTOINCREMENT";
			structStr.append(idAppendix);
		}

		if(i != lastElement)
			structStr.append(",");

		++i;
	}

	return structStr;
}

QString TableHandler::GetFieldsNames(QString tableName, TableStructMap &tableStruct, bool includeId)
{
	QString structStr = "";
	TableStructMap::Iterator i = tableStruct.begin();
	TableStructMap::Iterator lastElement = --tableStruct.end();

	while(i != tableStruct.end())
	{
		if(i.key() != "id" || includeId)
			structStr.append(QString("%1.%2").arg(tableName).arg(i.key()));

		if(i != lastElement)
			structStr.append(",");

		++i;
	}

	return structStr;
}

QString TableHandler::GetInsertValuesString(TableStructMap &tableStruct, int id, TableStructMap &itemData)
{
	if(tableStruct.count() != itemData.count())
	{
		qDebug() << "Wrong size" << tableStruct.count() << itemData.count();
		return QString();
	}

	QString fieldNamesStr = "(id, ";
	QString valuesStr = QString("(%1, ").arg(QString::number(id));
	TableStructMap::Iterator structIter = tableStruct.begin();
	TableStructMap::Iterator lastElement = --tableStruct.end();
	auto dataIter = itemData.begin();

	while(structIter != tableStruct.end())
	{
		fieldNamesStr.append(structIter.key());
		QString buf;

		auto dataType = dataIter.value().type();
		if(dataType == QVariant::String || dataType == QVariant::ByteArray)
			buf = QString("'%1'").arg(dataIter->toString());
		else if(dataType == QVariant::DateTime)
			buf = QString("%1").arg(dataIter->toDateTime().toSecsSinceEpoch());
		else
			buf = dataIter->toString();

		valuesStr.append(buf);

		if(structIter != lastElement)
		{
			fieldNamesStr.append(",");
			valuesStr.append(",");
		}
		else
		{
			fieldNamesStr.append(")");
			valuesStr.append(")");
		}

		++structIter;
		++dataIter;
	}

	return QString("%1 VALUES %2").arg(fieldNamesStr).arg(valuesStr);
}

QString TableHandler::GetUpdateValuesString(TableStructMap &tableStruct, int id)
{
	QString resultStr = "";
	TableStructMap::Iterator structIter = tableStruct.begin();
	TableStructMap::Iterator lastElement = --tableStruct.end();

	while(structIter != tableStruct.end())
	{
		resultStr.append(structIter.key()).append("=");

		if(structIter.value() == QVariant::String || structIter.value() == QVariant::ByteArray)
			resultStr.append(QString(":%1").arg(structIter.key().toUpper()));
		else
			resultStr.append(":").append(structIter.key().toUpper());

		if(structIter != lastElement)
			resultStr.append(",");

		++structIter;
	}

	resultStr.append(" where id=").append(QString::number(id));
	return resultStr;
}

QString TableHandler::GetUpdateValuesString(TableStructMap &tableStruct, int id, TableStructMap &itemData)
{
	if(tableStruct.count() != itemData.count())
	{
		qDebug() << "Wrong size" << tableStruct.count() << itemData.count();
		return QString();
	}

	QString resultStr = "";
	TableStructMap::Iterator structIter = tableStruct.begin();
	TableStructMap::Iterator lastElement = --tableStruct.end();
	auto dataIter = itemData.begin();

	while(structIter != tableStruct.end())
	{
		resultStr.append(structIter.key()).append("=");

		auto dataType = dataIter.value().type();
		if(dataType == QVariant::String || dataType == QVariant::ByteArray)
			resultStr.append(QString("'%1'").arg(dataIter.value().toString()));
		else
			resultStr.append(dataIter.value().toString());

		if(structIter != lastElement)
			resultStr.append(",");

		++structIter;
		++dataIter;
	}

	resultStr.append(" where id=").append(QString::number(id));
	return resultStr;
}

bool TableHandler::IsTableExists(QString tableName)
{
	QString queryStr = QString("pragma table_info(%1)").arg(tableName);
	QSqlQuery query = dataSource->ExecuteQuery(queryStr);
	return query.next();
}

void TableHandler::CombineWholeTableStruct()
{
	wholeTableStruct.clear();
	//	wholeTableStruct.unite(relationTablesStructs[tableName]);
	auto i = relationTablesStructs.begin();

	while(i != relationTablesStructs.end())
	{
		wholeTableStruct.unite(i.value());
		++i;
	}
}

bool TableHandler::IsTableHasRightStructure(QString tableName, TableStructMap &tableStruct)
{
	QString queryStr = QString("pragma table_info(%1)").arg(tableName);
	QSqlQuery query = dataSource->ExecuteQuery(queryStr);
	auto tableStructIter = tableStruct.begin();
	while(tableStructIter != tableStruct.end())
	{
		if(!query.next())
		{
			qDebug() << "Too few records!";
			return false;
		}

		auto name = query.value(1).toString();
		if(tableStructIter.key() != name)
		{
			qDebug() << QString("Field's '%1' name doesn't match for table %2").arg(name).arg(tableName);
			return false;
		}

		auto type = tableStructIter.value().type();
		auto sourceTypeString = query.value(2).toString();
		QString targetTypeString = dataBaseTypesNames.contains(type) ? dataBaseTypesNames[type] : QVariant::typeToName(type);
		if(sourceTypeString != targetTypeString)
		{
			qDebug() << QString("Field's '%1' type doesn't match for table %2").arg(name).arg(tableName);
			return false;
		}

		++tableStructIter;
	}

	return true;
}
