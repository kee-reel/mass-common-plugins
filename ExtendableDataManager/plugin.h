#pragma once


#include <QHash>
#include <QString>
#include <QWidget>
#include <QVariant>
#include <QPair>


#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Middleware/idatabase.h"
#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Middleware/iextendabledatamodel.h"

#include "tablehandler.h"
#include "model.h"

//! \addtogroup ExtendableDataManager_imp
//!  \{
class ExtendableDataManager : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "ExtendableDataManager" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	ExtendableDataManager();
	virtual ~ExtendableDataManager() override;

	// PluginBase interface
public:
	void onReferencesSet() override;
	void onReferencesListUpdated(Interface interface) override;

private:
	bool addExtention(Interface mainInterface, Interface relationInterface,
	        QMap<QString, QVariant> fields);
	bool deleteExtention(Interface mainName, Interface relationName);

private:
	QString lastError;
	ReferenceInstancePtr<IDataBase> m_dataSource;
	ReferenceInstancesListPtr<IDataExtention> m_dataExtentions;
	QMap<Interface, QSharedPointer<TableHandler>> tableHandlers;
};
//!  \}

